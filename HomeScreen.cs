using System;
using System.Windows.Forms;

namespace InventoryManagementProgram
{
    public partial class homeScreen : Form
    {
        inventoryScreen inventoryScreen = new inventoryScreen();
        searchScreen searchScreen = new searchScreen();
        addItemScreen addItemScreen = new addItemScreen();

        public homeScreen()
        {
            InitializeComponent();
        }

        private void viewInventoryButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            inventoryScreen.Show();
        }

        private void searchInventoryButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            searchScreen.Show();
        }

        private void addItemButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            addItemScreen.Show();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
